
# 码云+SourceTree 快速上手体验：协同开发、代码提交、冲突解决

作者：程序员小马

众所周知，git作为程序员的一门必修课，在日常的开发当中必不可少。但众多的命令让一些初学者对此望而生畏，但好在有众多的git管理软件可以供我们使用，图形化的设计
容易上手，体验更加，我们这里采用国外一款非常好用的git----SourceTree


### 环境准备
1.Windows/mac 安装Git并且配置（演示采用Windows版本）
2.安装SourceTree软件 官网：https://www.sourcetreeapp.com/


### 本地测试代码

1.我这里采用HBuilder X 创建一个基本的HTML模板 用于代码测试，我们来简单写一个东西，就写一个HelloWorld吧！
![Image text](https://gitee.com/mrc1999/HelloWorld/raw/master/img/1559221754(1).jpg)

### 码云仓库准备

1.登录码云，右上角加号新建仓库，然后就需要填写一些我们必要的信息，填写仓库地址、选择仓库私有或者开放、选择代码类型、点击创建即可完成
![Image text](https://gitee.com/mrc1999/HelloWorld/raw/master/img/1559222300(1).jpg)

2.本地创建仓库，同步我们刚才新建的HelloWorld 项目到云端，开始模拟代码的提交、拉取、同步以及冲突解决
![Image text](https://gitee.com/mrc1999/HelloWorld/raw/master/img/1559222709(1).jpg)

3.打开SourceTree软件 点击Create 创建一个本地仓库，位置选择刚才创建HelloWorld文件夹所在的位置，点击创建 提示是否继续创建，我们点击是
![Image text](https://gitee.com/mrc1999/HelloWorld/raw/master/img/1559222709(1).jpg)

4.创建好后如下面的图所示
![Image text](https://gitee.com/mrc1999/HelloWorld/raw/master/img/1559223208(1).jpg)

5.我们点击菜单栏仓库、然后选择仓库设置，新建一个远程仓库，把我们远端的仓库地址填写上去

![Image text](https://gitee.com/mrc1999/HelloWorld/raw/master/img/1559223362(1).jpg)

![Image text](https://gitee.com/mrc1999/HelloWorld/raw/master/img/1559223258(1).jpg)
![Image text](https://gitee.com/mrc1999/HelloWorld/raw/master/img/1559223655(1).jpg)

6.填写好之后，我们先做的事情就是要先把远端仓库更新到本地，因为远端仓库存在两个ReadME文件，就好比远端的仓库有两个苹果，我们仓库有一个梨，在两个仓库不相同的情况是无法进行操作的
  我们就先把远程仓库的两个苹果复制打本地 这样我们比远程仓库多一个梨，就可这个梨推送到远程仓库 实现远程和本地都是两个苹果一个梨
![Image text](https://gitee.com/mrc1999/HelloWorld/raw/master/img/1559224394(1).jpg)


### 上手操作，拉取远端代码到本地推送代码到远端（先拉取后推送）

1.点击菜单栏拉取，然后在弹框内选择我们要拉取的分支 只是一个简单的分支远端是origin 本地选择master主分支（默认创建）
![Image text](https://gitee.com/mrc1999/HelloWorld/raw/master/img/1559224589(1).jpg)


2.我们回过头去文件夹看一下，发现此时多了两个ReadMe文件
![Image text](https://gitee.com/mrc1999/HelloWorld/raw/master/img/1559224738(1).jpg)

3.开始把我们本地的代码提交到远端（两个苹果和一个梨送到远端去）
![Image text](https://gitee.com/mrc1999/HelloWorld/raw/master/img/1559225206(1).jpg)

4.填写作者以及邮箱信息后即可点击菜单来推送按钮
![Image text](https://gitee.com/mrc1999/HelloWorld/raw/master/img/1559225722(1).jpg)

5.检查云端代码的变化，发现多了一个index.html文件，但没有CSS这些文件夹 因为文件夹是空着的话它是不会计入的
![Image text](https://gitee.com/mrc1999/HelloWorld/raw/master/img/1559225925(1).jpg)

### 协同开发篇 
前言：代码部署好了 就是开发了，当然肯定不是你一个人在开发呀，所以我在这边模拟两个人开发的情况，和发生冲突时候的解决方案 模拟员工一号（我自己）开发后提交到云端，员工二号也在开发，各自开发自己的模块是没有问题的
假如改了同一个地方的代码的话就会发生冲突不可提交

1.上面的内容是员工一号搭建好框架现在要邀请员工二号一起开发，发送链接或者扫描二维码邀请即可
![Image text](https://gitee.com/mrc1999/HelloWorld/raw/master/img/1559226765(1).jpg)

2.二号员工拉取远端代码到他的电脑开始开发
![Image text](https://gitee.com/mrc1999/HelloWorld/raw/master/img/1559231204(1).jpg)

3.导入编辑器，随便写一点东西，还是先拉取后提交
![Image text](https://gitee.com/mrc1999/HelloWorld/raw/master/img/1559227993(1).jpg)
3.查看远端代码，发现已经有更新了，这时候一号员工也要写东西了，发现二号员工已经有提交了，那一号员工必须先拉取代码

![Image text](https://gitee.com/mrc1999/HelloWorld/raw/master/img/1559228752(1).jpg)

4.基本的协同开发就做到这里



### 冲突解决
1、假设一号员工修改了HelloWorld,二号员工也修改了这一行代码这样就会产生冲突
![Image text](https://gitee.com/mrc1999/HelloWorld/raw/master/img/1559228966(1).jpg)
![Image text](https://gitee.com/mrc1999/HelloWorld/raw/master/img/1559229006(1).jpg)

2、一号员工首先推送，远程仓库代码已经变成一号员工修改的
![Image text](https://gitee.com/mrc1999/HelloWorld/raw/master/img/1559229246(1).jpg)

3、此时二号员工也修改了一号员工的文件，他在推送的时候就会遇到代码冲突，因为两个人修改了同一处代码
![Image text](https://gitee.com/mrc1999/HelloWorld/raw/master/img/1559229345(1).jpg)


4、解决方案，二号员工首先拉取代码 ，确认冲突所在的地方 保留一个人的版本 我们保留他人版本 就是保留一号员工代码
![Image text](https://gitee.com/mrc1999/HelloWorld/raw/master/img/1559229474(1).jpg)
![Image text](https://gitee.com/mrc1999/HelloWorld/raw/master/img/1559229728(1).jpg)
![Image text](https://gitee.com/mrc1999/HelloWorld/raw/master/img/1559229823(1).jpg)

### 小结

git强大的地方还不至于这里，很多分支强大的管理，这只是一个简单的协同开发和冲突解决，还有分支开发以及其他的等着我们去学习和开发！！！



